#pragma once

#include <mutex>
#include <vector>
#include <map>
#include <memory>
#include <set>

#include "Message.hpp"
#include "IConsumer.hpp"

namespace pmmq {

    class Broker {
    public:
        Broker();

    public:
        int subscribe(const XIConsumer& _consumer);
        int unsubscribe(const XIConsumer& _consumer);

        int dispatch(XMessage& _message) const;

    private:
        using ConsumerVector = std::vector<XIConsumer>;
        using MessageTypeToConsumerVectorMap = std::map<wchar_t, ConsumerVector>;
        using ConsumerSet = std::set<XIConsumer>;
	private:
		mutable std::mutex mutex;

        MessageTypeToConsumerVectorMap mapping;

        ConsumerSet bookkeeping;
    };

    using XBroker = std::shared_ptr<Broker>;
}