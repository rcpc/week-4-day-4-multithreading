#pragma once
#include <atomic>
#include <queue>
#include <condition_variable>

class Consumer1 : public pmmq::IConsumer {
public:
	Consumer1(const wchar_t _message_type)
		: pmmq::IConsumer(_message_type)
		, consumed_count{ 0 }
		, stop_flag{ false }
		, messages_might_be_available{ false }
	{
		consume_thread = std::thread(&Consumer1::internal_consume, this);
	}

	~Consumer1() override {}

	void consume(pmmq::XMessage& _message) const override {
		/*while (true)
		{
			{
				std::lock_guard<std::mutex> lck(message_mutex);
				if (message == nullptr)
				{
					message = _message;
					break;
				}
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(2));
		}*/
		{
			std::lock_guard<std::mutex> lck(message_mutex);//1
			message_queue.push(_message);//2
			messages_might_be_available = true;
		}
		message_cv.notify_one();//3
	}

	int get_consumed_count() const {
		return consumed_count;
	}

	void end_consume() {
		stop_flag.store(true);
		if (consume_thread.joinable()) {
			messages_might_be_available = true;
			message_cv.notify_one();
			consume_thread.join();
		}
	}

private:

	void internal_consume() {
		while (!stop_flag.load()) {
			std::queue<pmmq::XMessage> local_message_queue;

			{
				//std::lock_guard<std::mutex> lck(message_mutex);

				/*if (message != nullptr)
				{
					std::swap(local_message, message);
					break;
				}*/

				std::unique_lock<std::mutex> ulck(message_mutex); //4
				message_cv.wait(ulck, [this] {
					return messages_might_be_available;
					});

				std::swap(message_queue, local_message_queue);
				messages_might_be_available = false;
			}

			assert(local_message_queue.size() > 0 || stop_flag.load());
			locked_consume(local_message_queue);
		}

		std::queue<pmmq::XMessage> local_message_queue;

		{
			std::lock_guard<std::mutex> lck(message_mutex);
			if (message_queue.size() > 0) {
				std::swap(message_queue, local_message_queue);
			}
		}

		assert(local_message_queue.size() > 0 || stop_flag.load());
		locked_consume(local_message_queue);

	}

	void locked_consume(std::queue<pmmq::XMessage>& _message_queue) {
		while (_message_queue.size() > 0) {
			const pmmq::XMessage message = _message_queue.front();
			_message_queue.pop();

			int dummy{ 0 };
			for (int i{ 0 }; i < 1000; ++i) {
				dummy++;
			}
			++consumed_count;
		}
	}

private:
	mutable int consumed_count;

	std::thread consume_thread;

	mutable std::queue<pmmq::XMessage> message_queue;

	mutable bool messages_might_be_available;

	mutable std::mutex message_mutex;

	mutable std::condition_variable message_cv;

	std::atomic_bool stop_flag;
};

using XConsumer1 = std::shared_ptr<Consumer1>;

